﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NoiseGenerator : MonoBehaviour
{
    ComputeShader genNoise;
    RenderTexture renderTexture;
    ComputeBuffer computeBuffer;


    List<Vector3> points;

    void generateRandomPoints(int xSamples, int ySamples, int zSamples)
    {
        //generate random points in some x by y by z grid
        System.Random r = new System.Random();
        for(int x = 0; x < xSamples; x++)
        {
            for(int y = 0; y < ySamples; y++)
            {
                for(int z = 0; z < zSamples; z++)
                {
                    //need to normalize these
                    float randX = (float) r.NextDouble();
                    float randY = (float)r.NextDouble();
                    float randZ = (float)r.NextDouble();

                    Vector3 point = new Vector3(randX, randY, randZ);
                    points.Add(point);
                }
            }
        }
    }

    void setupComputeShader()
    {
        int kernelHandle = genNoise.FindKernel("Noise");
        renderTexture = new RenderTexture(256, 256, 24);
        renderTexture.enableRandomWrite = true;
        renderTexture.Create();

        genNoise.SetTexture(kernelHandle, "noise", renderTexture);
        genNoise.Dispatch(kernelHandle,  256/ 8, 256 / 8, 1);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
