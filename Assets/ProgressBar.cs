﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public Slider slider;
    private EnvironmentManager em;
    public float maxSize = 583f;

    // Start is called before the first frame update
    void Start()
    {
        em = GameObject.FindObjectOfType<EnvironmentManager>();
    }

    // Update is called once per frame
    void Update()
    {
        RectTransform rt = transform.GetComponent<RectTransform>();
        Debug.Log(em.Health);
        slider.normalizedValue = em.Health;
    }
}
