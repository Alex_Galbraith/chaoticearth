﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolBelt : MonoBehaviour
{
    public GameObject reticle;
    private Tool currentTool;
    private GameObject reticleClone;

    void Start()
    {
        reticleClone = Instantiate(reticle, new Vector3(0, 0, 0), Quaternion.Euler(90, 0, 0));
        reticleClone.SetActive(false);
        currentTool = GetComponent<WindTool>();
    }

    public void SetTool(string tool)
    {
        currentTool = GetComponent($"{tool}Tool") as Tool;
        Debug.Log(currentTool);
    }

    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            reticleClone.SetActive(true);
            reticleClone.transform.position = hit.point;
        }
        else
        {
            reticleClone.SetActive(false);
        }

        if (Input.GetMouseButton(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            currentTool.PerformAction(hit);
        }
    }
}
