﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool : MonoBehaviour
{
    public virtual void Update(){ }
    public virtual void PerformAction(RaycastHit hit) { }
    public virtual void Awake() { }
}
