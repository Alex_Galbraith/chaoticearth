﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintTester : MonoBehaviour
{
    public SplatPainter _splatPainter;
    
    public Vector4 _color;
    [Range(1,100)]
    public float _radius = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0)){
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit)) {
                _splatPainter.AddBlot(new SplatPainter.PaintBlot(){
                    pos = hit.point,
                    color = _color,
                    radius = _radius
                });
            }
        }
    }
}
