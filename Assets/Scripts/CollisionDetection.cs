﻿using System;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    public event Action<Collision, GameObject> OnCollision;
    void OnCollisionEnter(Collision collision)
    {
        GameObject toolbelt = GameObject.FindGameObjectsWithTag("ToolBelt")[0];
        OnCollision?.Invoke(collision, this.gameObject);
    }
    
}
