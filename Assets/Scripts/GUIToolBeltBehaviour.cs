﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIToolBeltBehaviour : MonoBehaviour
{
    private ToolBelt toolBelt;

    public void HandleToolClick(string tool)
    {
        GameObject toolBeltManager = GameObject.FindGameObjectsWithTag("ToolBelt")[0];
        toolBelt = toolBeltManager.GetComponent<ToolBelt>();

        toolBelt.SetTool(tool);
    }
}
