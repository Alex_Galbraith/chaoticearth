﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

public class EnvironmentManager : MonoBehaviour
{
    public ComputeShader compute;
    public Collider collider;
    public Vector2Int textureSize = new Vector2Int(512, 512);
    public SplatPainter splat;
    private ComputeBuffer computeBuffer;
    private bool readyForRequest = true;
    private NativeArray<Color> colors;
    private Color[] colorArray;
    public Vector3 PassiveWindDirection = Vector3.right;
    public float Health;
    int kernelIndex;
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        kernelIndex = compute.FindKernel("CSMain");
        computeBuffer = new ComputeBuffer(512*512, sizeof(float)*4);
    }
    
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if(readyForRequest){
            readyForRequest = false;
            AsyncGPULoop();
        }
    }

    void AsyncGPULoop(){
        if(SystemInfo.supportsAsyncGPUReadback){
            UnityEngine.Rendering.AsyncGPUReadbackRequest request = UnityEngine.Rendering.AsyncGPUReadback.Request(computeBuffer, GPUCallback);
        }else{
            SyncGPUReadback();
        }
    }

    private void SyncGPUReadback()
    {
        if(colorArray == null)
            colorArray = new Color[512*512];
        computeBuffer.GetData(colorArray);
        Health = 0;
        for(int x = 64; x < 198; x += 16){
            for(int y = 64; y < 198; y += 16){
                Health += colorArray[512 * y + x].g;
            }  
        }
        Health /= (256/16) * (256/16);
        Health *= 4;
        //readback ~3 times a second
        StartCoroutine(UpdateReadyForRequest(0.3f));
    }

    private IEnumerator UpdateReadyForRequest(float delay){
        yield return new WaitForSecondsRealtime(delay);
        readyForRequest = true;
    }

    private void GPUCallback(AsyncGPUReadbackRequest obj)
    {
        if(!obj.hasError){
            Health = 0;
            colors = obj.GetData<Color>();
            for(int x = 64; x < 198; x += 16){
                for(int y = 64; y < 198; y += 16){
                    Health += colors[512 * y + x].g;
                }  
            }
            
            Health /= (256/16) * (256/16);
            Health *= 4;
        }
        readyForRequest = true;
    }



    public Color GetColor(Vector3 wpos){
        if(colors == null)
            return Color.clear;
        Ray ray = new Ray(wpos, Vector3.down * 1000);
        if(collider.Raycast(ray, out RaycastHit hit, 1000)){
            Vector2 uv = hit.textureCoord;
            return colors[(int)(uv.x * 512) + (int)(uv.y * 512) * 512];
        }
        return Color.clear;
    }

    public void PaintColor(Color color, Vector3 wpos, float radius){
        splat.AddBlot(new SplatPainter.PaintBlot(){
            pos = wpos,
            color = color,
            radius = radius
        });
    }

    //  public void AddInitialBuffers(CommandBuffer buffer){
    //     buffer.SetComputeTextureParam(compute,kernelIndex, "EnvironmentBuffer", splat.internalRenderTexture);
    //     buffer.SetComputeTextureParam(compute,kernelIndex, "ReadTexture", splat.alternate);
    //     buffer.DispatchCompute(compute,compute.FindKernel("Inital"), textureSize.x/8, textureSize.y/8, 1);
    // }

    public void AddCommandBuffers(CommandBuffer buffer){
        buffer.SetComputeTextureParam(compute,kernelIndex, "EnvironmentBuffer", splat.internalRenderTexture);
        buffer.SetComputeTextureParam(compute,kernelIndex, "ReadTexture", splat.alternate);
        buffer.SetComputeBufferParam(compute,kernelIndex, "OutputBuffer", computeBuffer);
        buffer.DispatchCompute(compute,kernelIndex, textureSize.x/8, textureSize.y/8, 1);
    }
}
