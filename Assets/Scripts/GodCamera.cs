﻿using UnityEngine;
using System.Collections;

public class GodCamera : MonoBehaviour
{
    public float rotateSpeed = 20;
    public float zoomSpeed = 20;
    public float xMax = 20;
    public float xMin = 0;
    public float zoomMax = 200;
    public float zoomMin = 80;

    public GameObject target;
    public Texture2D grabIcon;

    private float yRotation;
    private float xRotation;

    void Start()
    {
        yRotation = transform.eulerAngles.y;
        xRotation = transform.eulerAngles.x;
    }

    void Update()
    {
        MouseInputBehaviour();
    }

    void MouseInputBehaviour()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        if (Input.GetMouseButton(1))
        {
            float distanceFrom = Vector3.Magnitude(transform.position - target.transform.position);

            yRotation += Input.GetAxis("Mouse X") * rotateSpeed;
            xRotation -= Input.GetAxis("Mouse Y") * rotateSpeed;
            xRotation = ClampAngle(xRotation, xMin, xMax);

            Quaternion toRotation = Quaternion.Euler(xRotation, yRotation, 0);
            Quaternion rotation = toRotation;
           
            Vector3 negativeDistance = new Vector3(0, 0, -distanceFrom);
            Vector3 position = rotation * negativeDistance + target.transform.position;

            transform.rotation = rotation;
            transform.position = position;

            Cursor.SetCursor(grabIcon, Vector2.zero, CursorMode.Auto);
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            float distance = Vector3.Distance(transform.position, target.transform.position);
            Vector3 direction = Vector3.Normalize(transform.position - target.transform.position) * (distance * Input.GetAxis("Mouse ScrollWheel") * zoomSpeed);

            if (!(distance + (distance * Input.GetAxis("Mouse ScrollWheel") * zoomSpeed) < zoomMin) &&
                !(distance + (distance * Input.GetAxis("Mouse ScrollWheel") * zoomSpeed) > zoomMax))
            {
                transform.position += direction;
            }
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
        {
            angle += 360F;
        }

        if (angle > 360F)
        {
            angle -= 360F;
        }
            
        return Mathf.Clamp(angle, min, max);
    }
}