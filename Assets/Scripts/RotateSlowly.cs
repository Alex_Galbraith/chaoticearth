﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSlowly : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, 4), Space.Self);
    }
}
