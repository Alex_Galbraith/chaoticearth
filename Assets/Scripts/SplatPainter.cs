﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class SplatPainter : MonoBehaviour
{
    public RenderTexture renderTextureOut;
    [HideInInspector]
    public RenderTexture internalRenderTexture;
    [HideInInspector]
    public RenderTexture alternate;
    public Renderer renderer;
    public Material renderWith;
    public EnvironmentManager _environmentManager;
    CommandBuffer cb;
    // Start is called before the first frame update
    void Awake()
    {
        internalRenderTexture = new RenderTexture(512,512,1,RenderTextureFormat.ARGBFloat,RenderTextureReadWrite.Linear);
        internalRenderTexture.enableRandomWrite = true;
        alternate = new RenderTexture(internalRenderTexture);

        var clear = new CommandBuffer();
        clear.SetRenderTarget(internalRenderTexture);
        clear.ClearRenderTarget(false,true, new Color(0.1f,0,0.1f,0));
        // _environmentManager.AddInitialBuffers(clear);
        Graphics.ExecuteCommandBuffer(clear);
        
        
    }

    /// <summary>
    /// OnWillRenderObject is called for each camera if the object is visible.
    /// </summary>
    void OnWillRenderObject()
    {
        if(cb != null)
            Camera.main.RemoveCommandBuffer(CameraEvent.BeforeDepthTexture,cb);
        cb = new CommandBuffer();

        cb.SetRenderTarget(internalRenderTexture, 0);
        //drawn splats
        cb.DrawRenderer(renderer,renderWith);
        //copied to new texture
        cb.CopyTexture(internalRenderTexture,alternate);
        
        _environmentManager.AddCommandBuffers(cb);

        cb.CopyTexture(internalRenderTexture,renderTextureOut);


        Camera.main.AddCommandBuffer(CameraEvent.BeforeDepthTexture, cb);
        var temp = internalRenderTexture;
        internalRenderTexture = alternate;
        alternate = temp;
        renderWith.SetTexture("_MainTex", internalRenderTexture);
    }

    // Update is called once per frame
    void Update()
    {
        SendBlotsToGPU();
    }

    private void SendBlotsToGPU(){
        float[] radii = new float[10];
        Vector4[] points = new Vector4[10];
        Vector4[] colors = new Vector4[10];

        // Render up to 10 splats per frame.
        int i = 0;
        while (i < paintBlots.Count && i < 10)
        {
            points[i] = paintBlots[i].pos;
            colors[i] = paintBlots[i].color;
            radii[i] = paintBlots[i].radius;
            paintBlots[i].touched = true;
            i++;
        }
        
        for(int j = paintBlots.Count-1; j>=0; j--){
            if(paintBlots[j].touched)
                paintBlots.RemoveAt(j);
        }

        renderWith.SetFloatArray("_Radii", radii);
        renderWith.SetVectorArray("_Points", points);
        renderWith.SetVectorArray("_Colors", colors);

        renderWith.SetInt("_TotalPaints", i);
        renderWith.SetTexture("_MainTex",internalRenderTexture);
    }

    public void AddBlot(PaintBlot b)
    {
        paintBlots.Add(b);
    }
    internal List<PaintBlot> paintBlots = new List<PaintBlot>();
    

    public class PaintBlot
    {
        public Vector4 pos;
        public float radius;
        public Vector4 color;
        public bool touched = false;
    }
}
