﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorTool : Tool
{
    public GameObject meteor;
    public float meteorSpeed = 10;
    public ParticleSystem spawn;
    EnvironmentManager _environmentManager;
    private List<GameObject> _clonedMeteors = new List<GameObject>();
    private bool isActive = false;
    private RaycastHit hitPoint;
    [SerializeField]
    private float _cooldown = 0.1f;
    private float _lastUseTime;

    public override void Awake()
    {
        _environmentManager = GameObject.FindObjectOfType<EnvironmentManager>();
    }

    // Where we perform an action
    public override void PerformAction(RaycastHit hit)
    {
        if (Time.time > _lastUseTime + _cooldown)
        {
            _lastUseTime = Time.time;
            hitPoint = hit;
            var newMeteor = Instantiate(meteor, hitPoint.point + new Vector3(Random.Range(25,-25), 40, Random.Range(-25,25)), Quaternion.identity);
            newMeteor.GetComponent<CollisionDetection>().OnCollision += CollisionDetected;
            newMeteor.GetComponent<Rigidbody>().velocity = meteorSpeed * ((hit.point + Random.insideUnitSphere * 5) - newMeteor.transform.position).normalized; 
            _clonedMeteors.Add(newMeteor);

            isActive = true;
        }
    }

    public override void Update()
    {
    }

    public void CollisionDetected(Collision collision, GameObject meteor)
    {
        _clonedMeteors.Remove(meteor);
        Instantiate(spawn, meteor.transform.position, meteor.transform.rotation);
        meteor.GetComponent<Renderer>().enabled = false;
        meteor.GetComponent<Rigidbody>().isKinematic = true;
        Destroy(meteor,1f);
        isActive = false;
        
        _environmentManager.PaintColor(Color.red*10, collision.contacts[0].point, 7);
    }


}
