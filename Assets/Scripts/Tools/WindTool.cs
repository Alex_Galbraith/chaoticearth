﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindTool : Tool
{
    public float Speed = 10;
    public float SpeedMult = 10;
    public EnvironmentManager _environmentManager;
    public override void Awake()
    {
        _environmentManager = GameObject.FindObjectOfType<EnvironmentManager>();
    }

    // Where we perform an action
    public override void PerformAction(RaycastHit hit)
    {
        _environmentManager.PaintColor(new Color(0,0,-0.01f,0),hit.point, 10f);
        Cloud[] clouds = GameObject.FindObjectsOfType<Cloud>();
        foreach(Cloud cloud in clouds){
            var delta = hit.point -  cloud.transform.position;
            delta.y = 0;
            var mult = Vector2.SqrMagnitude(delta/SpeedMult)+0.01f;
            cloud.Velocity -= delta.normalized / (Mathf.Clamp(mult,0,Speed)) * Time.deltaTime;
        }

    }
}
