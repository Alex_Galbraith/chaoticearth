﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    public EnvironmentManager _environmentManager;
    public ToolBelt _toolBelt;
    [SerializeField]
    protected bool isRaining = true;
    [SerializeField]
    protected Animation _animation;
    public float LifeSpan = 10;
    private Vector3 scale;
    public float WetIntensity = 0.01f;
    public float ColdIntensity = 0.001f;
    public float RainChance = 0.1f;
    public Vector3 Velocity;

    // Start is called before the first frame update
    void Start()
    {
        scale = transform.localScale;
        _animation.Play("CloudIn");
        _environmentManager = GameObject.FindObjectOfType<EnvironmentManager>();
        _toolBelt = GameObject.FindObjectOfType<ToolBelt>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isRaining){
            if(Random.value < RainChance){
                Ray ray = new Ray(transform.position, Vector3.down * 1000);
                if(_environmentManager.collider.Raycast(ray, out RaycastHit hit, 1000)){
                    _environmentManager.PaintColor(new Color(-0.01f * ColdIntensity,0,0.1f * WetIntensity,0),hit.point, transform.lossyScale.x);

                }
            }
        }
        if(transform.localScale.x <=0){
            Destroy(this.gameObject);
        }
    }
    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        transform.position += Time.fixedDeltaTime * _environmentManager.PassiveWindDirection;
        transform.position += Time.fixedDeltaTime * Velocity;
        Velocity *= 0.9f;
        if(isRaining){
            transform.localScale -= scale * (1/LifeSpan) * Time.fixedDeltaTime;
        }
    }
}
