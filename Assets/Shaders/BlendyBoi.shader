﻿Shader "Unlit/BlendyBoi"
{
    Properties
    {
        _NormalTex  ("NormalTe", 2D) = "white" {}
        _ColdTex    ("ColdTex ", 2D) = "white" {}
        _DryTex     ("DryTex  ", 2D) = "white" {}
        _WetTex     ("WetTex  ", 2D) = "white" {}
        _HotTex     ("HotTex  ", 2D) = "white" {}
        _BlendTex   ("BlendTex", 2D) = "white" {}
        _AmbientColor ("Main Color", Color) = (1,1,1,1) //note: required but not used
        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Geometry" }
        LOD 100

        Pass
        {
            Tags { "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional"}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            #pragma multi_compile_fwdbase

            #include "UnityCG.cginc"
            // Files below include macros and functions to assist
			// with lighting and shadows.
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

            struct appdata
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
                float3 worldNormal : NORMAL;
                LIGHTING_COORDS(1,2)
            };

            sampler2D _NormalTex ;
            sampler2D _ColdTex   ;
            sampler2D _DryTex    ;
            sampler2D _WetTex    ;
            sampler2D _HotTex    ;
            sampler2D _BlendTex  ;
            float4 _AmbientColor;

            v2f vert (appdata_full v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                o.worldNormal = UnityObjectToWorldNormal(v.normal);	
                TRANSFER_SHADOW(o)
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 _NormalSample = tex2D(_NormalTex, i.uv);
                fixed4 _ColdSample = tex2D(_ColdTex  , i.uv);  
                fixed4 _DrySample = tex2D(_DryTex   , i.uv);   
                fixed4 _WetSample = tex2D(_WetTex   , i.uv);   
                fixed4 _HotSample = tex2D(_HotTex   , i.uv);   
                fixed4 _BlendSample = tex2D(_BlendTex , i.uv); 

                float shadow = SHADOW_ATTENUATION(i);

                fixed4 tempLerp = lerp(_ColdSample,_NormalSample, (_BlendSample.r*2));
                tempLerp = lerp(tempLerp,_HotSample, (_BlendSample.r-0.5)*2);
                fixed4 wetLerp = lerp(_DrySample,_NormalSample, (_BlendSample.b*2));
                wetLerp = lerp(wetLerp,_WetSample, (_BlendSample.b-0.5)*2);
                fixed4 result = lerp(tempLerp,wetLerp, 0.5 - abs(_BlendSample.r - 0.5) + abs(_BlendSample.b - 0.5));
                fixed4 col = result;
                col.a = 1;
                return  col * (shadow) * _LightColor0 + col * (1-shadow) * _AmbientColor  ;
            }
            ENDCG
        }
        UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}
