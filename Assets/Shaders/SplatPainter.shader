﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/SplatPainter"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    CGINCLUDE
    
    ENDCG

    SubShader
    {
        Tags { "RenderType"="Opaque" }
		LOD 100
		Cull Off
		ZWrite Off

        Pass
        {
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            

            #include "UnityCG.cginc"
            int _TotalPaints;
            float3 _Points[10];
            float4 _Colors[10];
            float _Radii[10];
            float4 _MainTex_TexelSize;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
                float3 wpos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = float4(v.uv * 2 -1,0,1);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                #if UNITY_UV_STARTS_AT_TOP
                #endif

                o.normal = mul( unity_ObjectToWorld, float4( v.normal, 0.0 ) ).xyz;
                o.wpos = mul(unity_ObjectToWorld, v.vertex ).xyz;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float4 currentSplat = tex2D(_MainTex, i.uv);
		        float4 wpos = float4(i.wpos,1);
                #if UNITY_UV_STARTS_AT_TOP
                    wpos.z = - wpos.z;
                #endif
                // sample the texture
                for( int i = 0; i < _TotalPaints; i++ ){
                    float distSq = pow(_Points[i].x -wpos.x,2) + pow(_Points[i].y-wpos.y,2) + pow(_Points[i].z-wpos.z,2);

                    // skip if outside of projection volume
                    if( distSq<=pow(_Radii[i],2) ){
                        currentSplat =   currentSplat + _Colors[i] * (1-(sqrt(distSq)/_Radii[i]));
                        currentSplat.r = max(min(currentSplat.r,1),0);
                        currentSplat.g = max(min(currentSplat.g,1),0);
                        currentSplat.b = max(min(currentSplat.b,1),0);
                    }

                }


                
                // apply fog
                return currentSplat;
            }
            ENDCG
        }
    }
}
