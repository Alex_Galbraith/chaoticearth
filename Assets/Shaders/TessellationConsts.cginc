/*

    AUTHOR: Alex Galbraith alexgalbraith.nz 

    Constant includes for tessellation

*/

#if !defined(TESSELLATION_CONST_INCLUDED)
#define TESSELLATION_CONST_INCLUDED

struct TessellationControlPoint
{
    float4 vertex : INTERNALTESSPOS;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
};

struct TessellationControlPoint_full
{
    float4 vertex : INTERNALTESSPOS;
    float4 tangent : TANGENT;
    float4 color : COLOR;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    float2 uv2 : TEXCOORD2;
    float2 uv3 : TEXCOORD3;
};

struct TessellationFactors
{
    float edge[3] : SV_TessFactor;
    float inside[1] : SV_InsideTessFactor;
};


#define DOMAIN_INTERPOLATE(fieldName) data.fieldName = \
		patch[0].fieldName * barycentricCoordinates.x + \
		patch[1].fieldName * barycentricCoordinates.y + \
		patch[2].fieldName * barycentricCoordinates.z;

#endif