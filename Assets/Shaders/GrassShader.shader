﻿
Shader "GrassShader"
{
	Properties
	{
		_NormalTex  ("NormalTex", 2D) = "white" {}
        _ColdTex    ("ColdTex ", 2D) = "white" {}
        _DryTex     ("DryTex  ", 2D) = "white" {}
        _WetTex     ("WetTex  ", 2D) = "white" {}
        _HotTex     ("HotTex  ", 2D) = "white" {}
		_EnrichMask("Enrich Mask", 2D) = "black" {}
		_GoalInverseDensity("Goal Inverse Density", Float) = 0.01
	}

	
	
	SubShader
	{
		
		Tags { "RenderType"="Opaque" }
		LOD 100
		Pass
		{
			Tags{ "LightMode"="ForwardBase" }
			ZWrite On
			ZTest Less
			Cull Off
			CGPROGRAM
			
			#pragma require geometry
			#pragma require tessellation tessHW
			#pragma multi_compile_fwdbase nolightmap nodirlightmap 
			#include "AutoLight.cginc"
			#include "UnityCG.cginc"
			#define fwdbase
			#define fwd
			#include "Lighting.cginc"
			#include "TessellationConsts.cginc"
			#include "Grass.cginc"

			#pragma vertex PassthroughVertex
			#pragma fragment frag
			#pragma geometry geom
			#pragma hull Hull
			#pragma domain Domain

			sampler2D _NormalTex ;
            sampler2D _ColdTex   ;
            sampler2D _DryTex    ;
            sampler2D _WetTex    ;
            sampler2D _HotTex    ;

			#include "Tessellation.cginc"

			fixed4 frag (v2f2 i) : COLOR
			{
				// sample the texture
                fixed4 _NormalSample = tex2D(_NormalTex, i.uv);
                fixed4 _ColdSample = tex2D(_ColdTex  , i.uv);  
                fixed4 _DrySample = tex2D(_DryTex   , i.uv);   
                fixed4 _WetSample = tex2D(_WetTex   , i.uv);   
                fixed4 _HotSample = tex2D(_HotTex   , i.uv);   
                fixed4 _BlendSample = tex2D(_EnrichMask , i.worldUV); 
 				fixed4 tempLerp = lerp(_ColdSample,_NormalSample, (_BlendSample.r*2));
                tempLerp = lerp(tempLerp,_HotSample, (_BlendSample.r-0.5)*2);
                fixed4 wetLerp = lerp(_DrySample,_NormalSample, (_BlendSample.b*2));
                wetLerp = lerp(wetLerp,_WetSample, (_BlendSample.b-0.5)*2);
                fixed4 result = lerp(tempLerp,wetLerp, 0.5 - abs(_BlendSample.r - 0.5) + abs(_BlendSample.b - 0.5));
                fixed4 col = result;
				float t = SHADOW_ATTENUATION(i);
				float3 rPos = UNITY_MATRIX_V[2].xyz;
				half3 diff = ShadeSH9(half4(rPos, 1));
				return (col * (_LightColor0*t + half4(diff,1)));
			}
			ENDCG
		}

		
        Pass
        {
			Name "ShadowCaster"
            Tags {"LightMode"="ShadowCaster"}
			Fog {Mode Off}
			ZWrite On 
			ZTest Less 
			Cull Off
			//Offset 1, 1
            CGPROGRAM
            #pragma multi_compile_shadowcaster
			#pragma multi_compile_shadowcollector
			#pragma require geometry
			#pragma require tessellation tessHW
			#define shadowcaster
            #include "UnityCG.cginc"
			#include "TessellationConsts.cginc"
			#include "Grass.cginc"

			#pragma vertex PassthroughVertex
			#pragma fragment frag
			#pragma geometry geom
			#pragma hull Hull
			#pragma domain Domain

			#include "Tessellation.cginc"
			

            float4 frag(v2f2 i) : SV_TARGET
            {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG

        }


	}
	FallBack "Standard"
}
