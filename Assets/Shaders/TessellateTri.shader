﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TessellateTri"
{
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Cull Off
		
		Pass 
		{ 
			CGPROGRAM
			#pragma vertex vert
			#pragma hull hull
			#pragma domain domain
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex : POSITION;
			};
			
			struct v2h
			{
				float4 pos : POSITION;
			};
			
			struct PatchTess
			{
				float Edges[3] : SV_TessFactor;
				float Inside[1] : SV_InsideTessFactor;
			};
			
			struct h2d
			{
				float4 pos : SV_POSITION;
			};
			
			struct d2f
			{
				float4 pos : POSITION;
				float3 uv : TEXCOORD0;
			};
			
			v2h vert (appdata_t v)
			{
				v2h o;
				o.pos = v.vertex;
				return o;
			}
			
			PatchTess ConstantHS(InputPatch<v2h, 3> tri)
			{
				PatchTess o;
				o.Edges[0] = o.Edges[1] = o.Edges[2] = o.Inside[0] = 3;
				return o;
			}
			
			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("ConstantHS")]
			[outputcontrolpoints(3)]
			
			h2d hull(InputPatch<v2h, 3> tri, uint i: SV_OutputControlPointID)
			{
				h2d o;
				o.pos = tri[i].pos.xyzw;
				return o;
			}
			
			[domain("tri")]
			d2f domain(PatchTess input, const OutputPatch<h2d, 3> tri, float3 uv : SV_DomainLocation)
			{
				d2f o;
				
				//Vert pos
				float4 pos = (uv.x * tri[0].pos) + (uv.y * tri[1].pos) + (uv.z * tri[2].pos);
				
				o.uv = uv;
				
				//Not important, just adding some displacement to see it working
				float3 value3 = normalize(uv) - uv;
				float value = value3.x + value3.y + value3.z;
				float3 v1 = float3(tri[1].pos.x - tri[0].pos.x, tri[1].pos.y - tri[0].pos.y, tri[1].pos.z - tri[0].pos.z);
				float3 v2 = float3(tri[2].pos.x - tri[0].pos.x, tri[2].pos.y - tri[0].pos.y, tri[2].pos.z - tri[0].pos.z);
				float3 calcNormal = normalize(cross(v1, v2) );
				pos.xyz += calcNormal * value * 0.75;
				
				o.pos = UnityObjectToClipPos(pos);
				return o;
			}
			
			fixed4 frag (d2f i) : SV_Target
			{  
				fixed4 col = fixed4(i.uv, 1.0);
				return col;
			}
			
			ENDCG
		}
	}
}