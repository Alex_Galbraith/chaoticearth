#if !defined(GRASS_INC)
#define GRASS_INC


#define MAX_GRASS_HEIGHT 1.4
#define MAX_GRASS_WIDTH 0.3
#define GRASS_INSET 0.1;
#define WIND fixed3(.1,0.01,0)*sin( center.x * 0.05 + _Time.y * 3 )*grassValue
#define MAX_VERTEX_COUNT 72


sampler2D _GrassTex;
sampler2D _EnrichMask;
float4 _GrassTex_ST;


float nrand(float2 uv){
	return frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453);
};
float nrand(float x, float y){
	return frac(sin(x*12.9898 + y*78.233) * 43758.5453);
};

void randomTriPos(float4 v1, float4 v2, float4 v3, float2 u1, float2 u2, float2 u3, int num, out float4 p, out float2 uvPos){
	float r1 = sqrt(nrand(v1.x + v1.z,v1.y + num*7.9372843));
	float r2 = (nrand(v2.x + v2.z * 3.191572246,v2.y + num));
	p = (1-(r1))*v1 + r1*(1-(r2))*v2 + r1*r2*v3;
	uvPos = (1-(r1))*u1 + r1*(1-(r2))*u2 + r1*r2*u3;
    

}

#define tess_Vertex TessVert
#define tess_const "PatchConstant"
#define tess_in tess_in
#define tess_v2f v2f
#define app_in appdata

#define TESS_POS vertex
#define TESS_WPOS wpos
#ifdef fwd
    // #define TESS_UV0 uv  
#endif
    #define TESS_UV3 uvE
#ifdef shadowcaster
    #define TESS_NORM normal
#endif



struct appdata
{
    float4 vertex : POSITION;
    float4 wpos : TEXCOORD2;
    #ifdef shadowcaster
        float3 normal : NORMAL;
    #endif
    #ifdef fwd
        // float2 uv : TEXCOORD1;
    #endif
        float2 uvE : TEXCOORD0;
};

struct tess_in
{
    float4 vertex : INTERNALTESSPOS;
    float4 wpos : TEXCOORD2;
    #ifdef shadowcaster
        float3 normal : NORMAL;
    #endif
    #ifdef fwd
        // float2 uv : TEXCOORD1;
    #endif
        float2 uvE : TEXCOORD0;
};

fixed _GoalInverseDensity;

TessellationFactors PatchConstant(InputPatch<tess_in, 3> patch)
{
    //Calculate area
    // float3 d1 = patch[1].wpos - patch[0].wpos;
    // float3 d2 = patch[2].wpos - patch[0].wpos;
    // float area = 0.5 * length(cross(d1, d2));
    // float subdivide = sqrt(floor(area / (_GoalInverseDensity+0.01f)));
    // TessellationFactors f;
    // f.edge[0] = subdivide;
    // f.edge[1] = subdivide;
    // f.edge[2] = subdivide;
    // f.inside = subdivide;
    TessellationFactors f;
    f.edge[0] = 1;
    f.edge[1] = 1;
    f.edge[2] = 1;
	f.inside[0] =  1;
    return f;
}

struct v2f2
{
    #ifdef fwd
        float2 uv : TEXCOORD0;
        float2 worldUV : TEXCOORD4;
        float4 pos : SV_POSITION;
        SHADOW_COORDS(1)
    #endif
    #ifdef shadowcaster
        //equiv to
        //defined int UnityCG.cginc
        //float3 vec : TEXCOORD0;
        //defined in HLSLSupport.cginc
        //float4 pos : VPOS
        V2F_SHADOW_CASTER;
        float3 normal : NORMAL;
    #endif
};

struct v2f
{
    #ifdef fwd
        float2 uv : TEXCOORD1;
    #endif
        float2 uvE : TEXCOORD0;
    float4 pos : INTERNALTESSPOS;
    float4 wpos : POSITION1;
};


v2f vert(appdata v)
{
    v2f o = (v2f) 0;
    o.pos = UnityObjectToClipPos(v.vertex);
    o.wpos = mul(unity_ObjectToWorld, v.vertex);
    #ifdef fwd
        // o.uv = TRANSFORM_TEX(v.uv, _GrassTex);
        o.uvE = v.uvE;
    #endif
    return o;
}


tess_v2f TessVert(tess_in v)
{
    tess_v2f o = (tess_v2f) 0;
    o.pos = UnityObjectToClipPos(v.vertex);
    o.wpos = mul(unity_ObjectToWorld, v.vertex);
    #ifdef fwd
        // o.uv = TRANSFORM_TEX(v.uv, _GrassTex);
    #endif
        o.uvE = v.uvE;
    return o;
}

[maxvertexcount(MAX_VERTEX_COUNT)]
void geom(triangle v2f input[3], inout TriangleStream<v2f2> OutputStream)
{

    v2f2 o = (v2f2) 0;
    float3 normal = normalize(cross(input[1].wpos.xyz - input[0].wpos.xyz, input[2].wpos.xyz - input[0].wpos.xyz)+float3(0,3,0));
    
    int i = 0;
    int j = 0;
    ///////////////////
    //Get grass data
    //////////////////
    float4 center;
    float2 uvCenter;
    randomTriPos(input[0].wpos, input[1].wpos, input[2].wpos, input[0].uvE, input[1].uvE, input[2].uvE, i * (j + i), center, uvCenter);
    center -= float4(normal, 0) * GRASS_INSET;
    #ifdef shadowcaster
        float3 rPos = mul(unity_CameraToWorld,fixed4(0,0,1,0)).xyz;
        center -= float4(rPos,0) *0.01f;
    #else
        float3 rPos = UNITY_MATRIX_V[2].xyz;
    #endif
    float4 enrich = tex2Dlod(_EnrichMask, float4(uvCenter, 0, 0));
    float grassValue = enrich.g*2;//1 - min(1,(pow((enrich.r-0.5)*2,2) + (pow((enrich.b-0.5)*2,2))));
    float4 tip = float4((center + normal * (nrand(normal.xy + fixed2(j, i)) * 0.2 + 0.8) * MAX_GRASS_HEIGHT * grassValue) + WIND, 1);
            
            
    float4 tangent = float4(normalize(cross(normal, rPos)), 0);
    float4 off = tangent * grassValue * (nrand(normal.xy + fixed2(j, i)) * 0.5 + 0.5) * MAX_GRASS_WIDTH;
    //need this for TRANSFER_SHADOW
    appdata v;
    #ifdef shadowcaster
        v.normal = rPos;
    #endif

    v.vertex = mul(unity_WorldToObject, tip);

    #ifdef fwd
        o.pos = UnityWorldToClipPos(tip);
        o.uv = fixed2(0.5, 1);
        TRANSFER_SHADOW(o)
    #endif
    #ifdef shadowcaster
        v.normal = rPos;
        TRANSFER_SHADOW_CASTER_NORMALOFFSET(o);
    #endif
    #ifdef fwd
        o.worldUV = uvCenter;
    #endif

    OutputStream.Append(o);

    v.vertex = mul(unity_WorldToObject, center - off);
            
    #ifdef fwd
        o.pos = UnityWorldToClipPos(center - off);
        o.uv = fixed2(0, 0);
        TRANSFER_SHADOW(o)
    #endif
    #ifdef shadowcaster
        v.normal = rPos;
        TRANSFER_SHADOW_CASTER_NORMALOFFSET(o);
    #endif


    OutputStream.Append(o);

    v.vertex = mul(unity_WorldToObject, center + off);

    #ifdef fwd		
        o.pos = UnityWorldToClipPos(center + off);

        o.uv = fixed2(1, 0);
        TRANSFER_SHADOW(o)
    #endif
    #ifdef shadowcaster
        v.normal = rPos;
        TRANSFER_SHADOW_CASTER_NORMALOFFSET(o);
    #endif


	OutputStream.Append(o);

    OutputStream.RestartStrip();
}
#endif