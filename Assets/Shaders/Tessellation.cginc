/*

AUTHOR: Alex Galbraith alexgalbraith.nz 

MUST DEFINE
app_in      : vertex inout struct
tess_in     : same as app_in but using vertex : INTERNALTESSPOS
tess_v2f    : output from tess_Vertex
vertex      : PassthroughVertex 
tess_Vertex : Vertex program that operates on tess_v2f
tess_const  : constant output function of type TessellationFactors as string e.g., "MyConstFunction"

The following should be defined if used
TESS_POS
TESS_NORMAL
TESS_TANGENT
TESS_COLOR
TESS_WPOS : Automatically convert the vertex position to world position and store it in TESS_WPOS
TESS_UV[0-4]

For example, the struct
struct MyTessellationIn {
    float4 vertex : SV_POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
    float2 uv2 : TEXCOORD1;
}

would have tess_v2f

struct MyTessellationV2F {
    float4 vertex : INTERNALTESSPOS;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
    float2 uv2 : TEXCOORD1;
}

and defines

#define TESS_POS vertex
#define TESS_NORMAL normal
#define TESS_UV0 uv
#define TESS_UV1 uv2

#define tess_in  MyTessellationIn
#define tess_v2f MyTessellationV2F



*/

#if !defined(TESSELLATION_INCLUDED)
#define TESSELLATION_INCLUDED

#include "TessellationConsts.cginc"

[UNITY_domain("tri")]
[UNITY_outputcontrolpoints(3)]
[UNITY_outputtopology("triangle_cw")]
[UNITY_partitioning("integer")]
[UNITY_patchconstantfunc(tess_const)]
tess_in Hull(
	InputPatch<tess_in, 3> patch,
	uint id : SV_OutputControlPointID
)
{
    return patch[id];
}




tess_in PassthroughVertex(app_in v)
{
    tess_in o;

    #ifdef TESS_POS
        o.TESS_POS = v.TESS_POS;
    #endif
    #ifdef TESS_NORM
        o.TESS_NORM = v.TESS_NORM;
    #endif
    #ifdef TESS_WPOS
        o.TESS_WPOS = mul(unity_ObjectToWorld, v.TESS_POS);
    #endif
    #ifdef TESS_UV0
         o.TESS_UV0 = v.TESS_UV0;
    #endif
    #ifdef TESS_UV1
        o.TESS_UV1 = v.TESS_UV1;
    #endif
    #ifdef TESS_UV2
        o.TESS_UV2 = v.TESS_UV2;
    #endif
    #ifdef TESS_UV3
        o.TESS_UV3 = v.TESS_UV3;
    #endif
    #ifdef TESS_UV4
        o.TESS_UV4 = v.TESS_UV4;
    #endif
    #ifdef TESS_COLOR
        o.TESS_COLOR = v.TESS_COLOR;
    #endif
    #ifdef TESS_TANGENT
        o.TESS_TANGENT = v.TESS_TANGENT;
    #endif

    return o;
}

[UNITY_domain("tri")]
tess_v2f Domain(
	TessellationFactors factors,
	OutputPatch<tess_in, 3> patch,
	float3 barycentricCoordinates : SV_DomainLocation
)
{
    tess_in data;
    #ifdef TESS_POS
        DOMAIN_INTERPOLATE(TESS_POS);
    #endif
    #ifdef TESS_NORMAL
        DOMAIN_INTERPOLATE(TESS_NORMAL);
    #endif
    #ifdef TESS_WPOS
        DOMAIN_INTERPOLATE(TESS_WPOS);
    #endif
    #ifdef TESS_UV0
        DOMAIN_INTERPOLATE(TESS_UV0);
    #endif
    #ifdef TESS_UV1
        DOMAIN_INTERPOLATE(TESS_UV1);
    #endif
    #ifdef TESS_UV2
        DOMAIN_INTERPOLATE(TESS_UV2);
    #endif
    #ifdef TESS_UV3
        DOMAIN_INTERPOLATE(TESS_UV3);
    #endif
    #ifdef TESS_UV4
        DOMAIN_INTERPOLATE(TESS_UV4);
    #endif
    #ifdef TESS_COLOR
        DOMAIN_INTERPOLATE(TESS_COLOR);
    #endif
    #ifdef TESS_TANGENT
        DOMAIN_INTERPOLATE(TESS_TANGENT);
    #endif
    

    return tess_Vertex(data);
}

#endif