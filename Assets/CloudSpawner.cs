﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour
{
    public Cloud cloudPrefab;
    public float CloudSpawnChance = 0.01f;
    private float _realSpawnChance;
    // Start is called before the first frame update
    void Start()
    {
        _realSpawnChance = CloudSpawnChance;
    }

    // Update is called once per frame
    void Update()
    {
        _realSpawnChance += Time.deltaTime * CloudSpawnChance;
        if(Random.value < _realSpawnChance){
            Vector2 pos = Random.insideUnitCircle * 50;
            Instantiate(cloudPrefab, new Vector3(pos.x,transform.position.y,pos.y),Quaternion.identity);
            _realSpawnChance = CloudSpawnChance;
        }
    }
}
